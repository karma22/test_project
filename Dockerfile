FROM shephexd/python:3.6

RUN apt-get update && apt-get install nginx -y \
    && pip install uwsgi pip --upgrade \
    && apt-get install -y logrotate \
    && fc-cache -fv \
    && apt-get install dos2unix

ARG user=fount
ARG group=fount
ARG uid=1000
ARG gid=1000
ARG WEBAPP_HOME=/webapp/server
ARG UWSGI_HOME=/webapp/uwsgi
ARG UWSGI_MODULE=test_project.wsgi
ARG NGINX_SET_REAL_IP_FROM="172.18.0.0/16"
ARG DEPLOY_USER=$DEPLOY_USER

ENV WEBAPP_HOME=$WEBAPP_HOME \
    UWSGI_WORK_HOME=$UWSGI_HOME \
    UWSGI_SOCKET=/tmp/webapp.sock \
    UWSGI_PID=/tmp/webapp.pid \
    UWSGI_CHDIR=$WEBAPP_HOME \
    UWSGI_MODULE=$UWSGI_MODULE \
    UWSGI_PROCESSES=8 \
    PROJECT_ENV="development"

ADD ./requirements.txt $WEBAPP_HOME/requirements.txt

RUN mkdir -p $WEBAPP_HOME/sub_modules \
  && mkdir -p $UWSGI_WORK_HOME \
  && mkdir -p /webapp/nginx \
  && groupadd -g ${gid} ${group} \
  && useradd -d "$WEBAPP_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user} \
  && ln -sf /dev/stdout /var/log/nginx/access.log \
  && ln -sf /dev/stderr /var/log/nginx/error.log \
  && rm -rf /var/lib/nginx \
  && ln -sf /webapp/nginx /var/lib/nginx

ADD . $WEBAPP_HOME
WORKDIR $WEBAPP_HOME

RUN pip install -r requirements.txt

RUN chown -R ${uid}:${gid} $WEBAPP_HOME \
  && chown -R ${uid}:${gid} $UWSGI_WORK_HOME \
  && chown -R ${uid}:${gid} /webapp/nginx \
  && mv ./conf/nginx.conf /etc/nginx/nginx.conf \
  && mv ./conf/webapp.conf /etc/nginx/conf.d/webapp.conf \
  && mv ./conf/uwsgi.ini /webapp/uwsgi/uwsgi.ini \
  && sed -i 's,NGINX_SET_REAL_IP_FROM,'"$NGINX_SET_REAL_IP_FROM"',g' /etc/nginx/nginx.conf \
  && chmod 755 ./conf/run.sh \
  && dos2unix ./conf/run.sh

USER ${user}
CMD ["/bin/bash", "./conf/run.sh"]

